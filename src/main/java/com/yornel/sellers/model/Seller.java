package com.yornel.sellers.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Seller {

    @Id
    private Long id;

    @Column
    private String name;

    @Column
    private String email;

    @Column
    private Double commission;

    @Column
    private String avatar;

    @Column
    private Boolean active;

    @Column(name = "current_commissions")
    private Double currentCommissions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Double getCurrentCommissions() {
        return currentCommissions;
    }

    public void setCurrentCommissions(Double currentCommissions) {
        this.currentCommissions = currentCommissions;
    }
}
