package com.yornel.sellers.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class Sale {

    @Id
    private Long id;

    @Column(name = "seller_id")
    private Long sellerId;

    @Column
    private Timestamp date;

    @Column
    private String name;

    @Column
    private Double amount;

    @Column(name = "commission_obtained")
    private Double commissionObtained;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public Sale setSellerId(Long sellerId) {
        this.sellerId = sellerId;
        return this;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getCommissionObtained() {
        return commissionObtained;
    }

    public void setCommissionObtained(Double commissionObtained) {
        this.commissionObtained = commissionObtained;
    }
}
