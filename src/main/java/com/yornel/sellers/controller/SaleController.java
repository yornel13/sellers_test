package com.yornel.sellers.controller;

import com.yornel.sellers.model.Sale;
import com.yornel.sellers.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(path = "sales")
public class SaleController {

    @Autowired
    private SaleService service;

    @GetMapping
    public List<Sale> getAll() {
        return service.getAll();
    }

    @GetMapping("{id}")
    public Sale findById(@PathVariable("id") Long saleId) {
        return service.findById(saleId);
    }

    @GetMapping("seller/{id}")
    public List<Sale> getAllBySellerId(@PathVariable("id") Long sellerId) {
        return service.getAllBySellerId(sellerId);
    }

    @PostMapping(value = "upload", consumes = "multipart/form-data")
    public void upload(@RequestParam("file") MultipartFile file) throws IOException {
        if (file != null) {
            service.saveAll(file.getInputStream());
        }
    }
}
