package com.yornel.sellers.repository;

import com.yornel.sellers.model.Sale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

public interface SaleRepository extends JpaRepository<Sale, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM sale")
    List<Sale> getAll();

    @Query(nativeQuery = true, value = "SELECT * FROM sale WHERE id = :sale_id")
    Optional<Sale> findById(@Param("sale_id") Long saleId);

    @Query(nativeQuery = true, value = "SELECT * FROM sale WHERE seller_id = :seller_id")
    List<Sale> getAllBySellerId(@Param("seller_id") Long sellerId);

    @Modifying
    @Query(nativeQuery = true, value =
            "INSERT INTO sale (seller_id, date, name, amount, commission_obtained) " +
            "VALUES (:seller_id, :date, :name, :amount, ((:amount / 100) * " +
                    "(SELECT commission FROM seller WHERE id = :seller_id) )) ")
    Integer insert(@Param("seller_id") Long sellerId,
                   @Param("date") Timestamp date,
                   @Param("name") String name,
                   @Param("amount") Double amount);
}
