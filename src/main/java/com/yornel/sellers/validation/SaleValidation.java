package com.yornel.sellers.validation;

import com.yornel.sellers.util.DateUtils;

import java.util.Map;

public class SaleValidation {

    private SaleValidation() {

    }

    public static boolean validate(Map<String, String> sale) {

        boolean valid = true;

        if (!sale.containsKey("sellerId")
                || !sale.containsKey("name")
                || !sale.containsKey("amount")
                || !sale.containsKey("date")
                || !isNumeric(sale.get("sellerId"))
                || !isAlphanumeric(sale.get("name"))
                || !DateUtils.isValidTimestamp(sale.get("date"))) {
            valid = false;
        }

        return valid;
    }

    public static boolean isAlphanumeric(String value) {
        return value.matches("[a-zA-Z0-9]+");
    }

    public static boolean isNumeric(String value) {
        return value.matches("[0-9]+");
    }
}
