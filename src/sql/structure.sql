CREATE DATABASE IF NOT EXISTS `sellers`;

CREATE TABLE `sellers`.`seller` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `commission` decimal(3,2) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `sellers`.`sale` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `seller_id` bigint(20) NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `amount` decimal(8,2) DEFAULT NULL,
  `commission_obtained` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seller_forgein_idx` (`seller_id`),
  CONSTRAINT `seller_forgein` FOREIGN KEY (`seller_id`) REFERENCES `seller` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
