package com.yornel.sellers;


import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.yornel.sellers.util.CsvUtils;


@SpringBootTest
public class CsvUtilsTest {


	@Autowired
    ResourceLoader resourceLoader;
	@Test
	public void readOK() throws IOException {
		
		
		Resource resource = resourceLoader.getResource("classpath:demo.csv");

		InputStream input = resource.getInputStream();
		
		List<Map<String, String>> map = CsvUtils.read(input);
		assertTrue(map!=null && map.size()>0);
	}
	
	
}
