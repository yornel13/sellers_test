package com.yornel.sellers;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.yornel.sellers.util.CsvUtils;
import com.yornel.sellers.util.DateUtils;

@SpringBootTest
public class DateUtilsTest {

	@Autowired
    ResourceLoader resourceLoader;
	@Test
	public void getCurrentTimestamp() throws IOException {
		
		Timestamp now = new Timestamp(new Date().getTime());
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Timestamp now_now = DateUtils.getCurrentTimestamp();
		
		assertTrue(now_now != null && now.before(now_now));
		
	}
	
	@Test
	public void isValidTimestamp_OK() {
		assertTrue(DateUtils.isValidTimestamp("2020-02-01 12:12:12"));
	}
	
	@Test
	public void isValidTimestamp_Error1() {
		
		assertTrue(!DateUtils.isValidTimestamp("-01 12:12:12"));
	}
	
	@Test
	public void isValidTimestamp_Error2() {
		assertTrue(!DateUtils.isValidTimestamp(""));
	}
	
	
	@Test
	public void toTimestamp_OK() {
		
		assertTrue(DateUtils.toTimestamp("2020-12-01 12:12:12")!=null);
	}
	
	@Test
	public void toTimestamp_Error() {
		assertTrue(DateUtils.toTimestamp("-01 12:12:12")==null);
	}
}
